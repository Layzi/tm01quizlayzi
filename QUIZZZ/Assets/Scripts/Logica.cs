﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Quiz
{
    public string texto;
}

public class ClassResposta : Quiz
{
    public bool ecorreta;
    
    public ClassResposta(string a, bool b)
    {
        this.texto = a;
        this.ecorreta = b;
    }
}

public class ClassPergunta : Quiz
{ // CLASS PERGUNTAS
    public int pontos;
    public ClassResposta[] respostas;

    // METODO É PARA CRIAR AS PERGUNTAS
    public ClassPergunta(string t, int p)
    {
        this.texto = t;
        this.pontos = p;
    }

    // METODO PARA ADD RESPOSTA
    public void AddResposta(string a, bool b)
    {
        if (respostas == null)
        {
            respostas = new ClassResposta[4];
        }

        for (int i = 0; i < respostas.Length; i++)
        {
            if (respostas[i] == null)
            {
                respostas[i] = new ClassResposta(a, b);
                break;
            }
        }
    }

    // LISTA ou EXIBIR as respostas
    public void ListarRespostas()
    {
        if (respostas != null && respostas.Length > 0)
        {
            foreach (ClassResposta r in respostas)
            {
                Debug.Log(r);
            }
        }
    }

    // Checar resposta correta
    public bool ChecarRespostaCorreta(int pos)
    {
        if (respostas != null && respostas.Length > pos && pos >= 0)
        {
            return respostas[pos].ecorreta;
        }
        else
        {
            return false;
        }
    }

} // FIM CLASS PERGUNTAS


// CLASS LÓGICA - PRINCIPAL
public class Logica : MonoBehaviour
{ // INICIO CLASS LOGICA

    public Text tituloPergunta;
    public Button[] respostaBtn = new Button[4];
    public List<ClassPergunta> PerguntasLista = new List<ClassPergunta>();

    private ClassPergunta perguntaAtual;

    public GameObject PainelCorreto;
    public GameObject PainelErrado;
    public GameObject PainelFinal;
    private int contagemPerguntas = 0;
    private float tempo;
    private bool carregarProximaPergunta;
    private int pontos = 0;


    void Start()
    { // METODO START

        ClassPergunta p1 = new ClassPergunta("1. Em um estudo foi descoberto que, em uma simples lavagem, uma peça do vestuário desta fibra pode soltar até 1900 fibras de micro plástico. Poluindo o oceano", 5);
        p1.AddResposta("Algodão", false);
        p1.AddResposta("Poliéster", true);
        p1.AddResposta("Poliamida/Náilon", false);
        p1.AddResposta("Acrílico", false);

        ClassPergunta p2 = new ClassPergunta("2.  Para a produção de uma simples calça jeans é gasto _________ de agua.", 3);
        p2.AddResposta("13 mil litros", false);
        p2.AddResposta("9 mil litros", false);
        p2.AddResposta("11 mil litros", true);
        p2.AddResposta("7 mil litro", false);

        ClassPergunta p3 = new ClassPergunta("3.E para a produção de um par de sapatos de couro a quantidade de água é de", 6);
        p3.AddResposta("17 mil litros", true);
        p3.AddResposta("16 mil litros", false);
        p3.AddResposta("18 mil litros", false);
        p3.AddResposta("15 mil litros", false);

        ClassPergunta p4 = new ClassPergunta("4. Você sabe qual é a média da produção média de roupas no Brasil? Segundo (ABIT, 2017)", 2);
        p4.AddResposta("4,7 BILHÕES", false);
        p4.AddResposta("6,7 BILHÕES", false);
        p4.AddResposta("5,7 BILHÕES.", true);
        p4.AddResposta(" 3,7 BILHÕES", false);

        ClassPergunta p5 = new ClassPergunta("5. Desta produção qual a quantidade gerada de resíduos(lixo) em São Paulo?", 5);
        p5.AddResposta("160 mil toneladas", false);
        p5.AddResposta("170 mil toneladas", true);
        p5.AddResposta("150 mil toneladas", false);
        p5.AddResposta("120 mil toneladas", false);

        ClassPergunta p6 = new ClassPergunta("6. Dos resíduos gerados em São Paulo, qual a quantidade que vai parar em aterros sanitários?", 5);
        p6.AddResposta("70 mil", false);
        p6.AddResposta("80 mil.", false);
        p6.AddResposta("90 mil", false);
        p6.AddResposta("100 mil", true);

        ClassPergunta p7 = new ClassPergunta("7.A produção de qual destas matérias primas da indústria têxtil é responsável por 24% de todo consumo de inseticidas e 11% dos pesticidas da agricultura?", 5);
        p7.AddResposta("Linho", false);
        p7.AddResposta("Cânhamo", false);
        p7.AddResposta("Algodão", true);
        p7.AddResposta("Lã", false);

        ClassPergunta p8 = new ClassPergunta("8. Segundo a pesquisa The Global Slavery Index 2018, da fundação Walk Free, divulgada recentemente, a moda é a segunda categoria de exportação que mais explora o trabalho forçado. Cerca de ____________  de pessoas no mundo estão na situação de trabalho forçado", 5);
        p8.AddResposta("21,9 milhões", false);
        p8.AddResposta("22,9 milhões", false);
        p8.AddResposta("24,9 milhões", true);
        p8.AddResposta("26,9 milhões", false);

        ClassPergunta p9 = new ClassPergunta("9. O processo de criar algo novo a partir de itens antigos é chamada de", 5);
        p9.AddResposta("Zero waste", false);
        p9.AddResposta("modaeco", false);
        p9.AddResposta("Reroupa", false);
        p9.AddResposta("UpCycling", true);

        ClassPergunta p10 = new ClassPergunta("10.  Foi desenvolvido um aplicativo onde você pode ver a reputação da marca em relação ao envolvimento com trabalho escravo, esse app é o", 5);
        p10.AddResposta("Eco Moda", false);
        p10.AddResposta("Fashion Free", false);
        p10.AddResposta("Moda Sustentavel", false);
        p10.AddResposta("Moda Livre", true);



        PerguntasLista.Add(p1);
        PerguntasLista.Add(p2);
        PerguntasLista.Add(p3);
        PerguntasLista.Add(p4);
        PerguntasLista.Add(p5);
        PerguntasLista.Add(p6);
        PerguntasLista.Add(p7);
        PerguntasLista.Add(p8);
        PerguntasLista.Add(p9);
        PerguntasLista.Add(p10);

        perguntaAtual = p1;

        ExibirPerguntasNoQuiz();


    } // FIM METODO START

    void Update()
    {
        if (carregarProximaPergunta == true)
        {
            tempo += Time.deltaTime;
            if (tempo > 3)
            {
                tempo = 0;
                carregarProximaPergunta = false;
                ProximaPergunta();
            }
        }
    }


    private void ExibirPerguntasNoQuiz()
    {
        tituloPergunta.text = perguntaAtual.texto;
        respostaBtn[0].GetComponentInChildren<Text>().text = perguntaAtual.respostas[0].texto;
        respostaBtn[1].GetComponentInChildren<Text>().text = perguntaAtual.respostas[1].texto;
        respostaBtn[2].GetComponentInChildren<Text>().text = perguntaAtual.respostas[2].texto;
        respostaBtn[3].GetComponentInChildren<Text>().text = perguntaAtual.respostas[3].texto;
    }

    public void ProximaPergunta()
    {
        contagemPerguntas++;
        if (contagemPerguntas < PerguntasLista.Count)
        {
            perguntaAtual = PerguntasLista[contagemPerguntas];
            ExibirPerguntasNoQuiz();
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
        }
        else
        {
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
            PainelFinal.SetActive(true);
            PainelFinal.transform.GetComponentInChildren<Text>().text = "Pontos: " + pontos.ToString();
        }
    }

    public void ClickResposta(int pos)
    {
        if (perguntaAtual.respostas[pos].ecorreta)
        {
            //Debug.Log("Resposta certa!!!");
            PainelCorreto.SetActive(true);
            pontos += perguntaAtual.pontos;
                
        }
        else
        {
            //Debug.Log("Resposta errada!!!");
            PainelErrado.SetActive(true);
        }

        carregarProximaPergunta = true;
    }

    public void IniciarQuiz()
    {
        SceneManager.LoadScene("Quiz");
    }

    public void MenuInicial()
    {
        SceneManager.LoadScene("MenuInicial");
    }

    public void Credito()
    {
        SceneManager.LoadScene("Credito");
    }

    public void SairJogo()
    {
        Application.Quit();
    }




}// FIM CLASS LOGICA
